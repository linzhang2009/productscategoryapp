﻿using Products.Library;
using Products.Library.ProductsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Products.Web.Controllers
{
    public class CategoryController : ApiController
    {
        private CategoriesRepository categoriesRepository;

        //Constructor
        public CategoryController()
        {
            this.categoriesRepository = new CategoriesRepository();
        }

        // api/categories
        public List<Category> GetAll()
        {
            var CategoriesList = categoriesRepository.GetAllCategories();
            return CategoriesList;
        }

    }
}
