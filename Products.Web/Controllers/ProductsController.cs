﻿using Products.Library;
using Products.Library.ProductsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Products.Web.Controllers
{
    public class ProductsController : ApiController
    {

        private ProductsRepository productRepository;

        //Constructor
        public ProductsController()
        {
            this.productRepository = new ProductsRepository();
        }


        //api/products
       // [Authorize(Users = "bill")]
        public List<Product> GetAll()
        {
            var productList = productRepository.GetAllProducts();
            return productList;
        }

        //api/product/id
        //route settings e.g., api/products/1234 or use api/products?id=1234
        // routeTemplate: "api/{controller}/{id}",
        public Product GetById(int productId)
        {
            var product = productRepository.GetProductById(productId);
            if (product == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return product;
        }


        //api/products?cat=electronics
        public List<Product> GetProductsCategory(string cat)
        {
            var products = productRepository.GetProductsByCategory(cat).ToList();
            return products;
        }

        //POST for Create  // api/products 
        public HttpResponseMessage PostProduct(Product prod)
        {
            Product p = productRepository.Add(prod);
            if(prod!=null)
            {
                var response = Request.CreateResponse<Product>(HttpStatusCode.Created, prod);
                string uri = Url.Link("DefaultApi", new { id=prod.ProductName});
                response.Headers.Location = new Uri(uri);
                return response;
            }
            else
                throw new HttpResponseException(HttpStatusCode.InternalServerError);            
        }

        //PUT FOR UPDATE // api/products/id 
        public void PutProduct(Product prod)
        {
            var res = productRepository.Update(prod);
            if(res==null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }


        // DELETE api/Book/5
        public HttpResponseMessage DeleteProduct(int productId)
        {
            Product product = productRepository.GetProductById(productId);
            if (product == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            productRepository.Delete(productId);
            return Request.CreateResponse(HttpStatusCode.OK, product);
        }

    }
}
