﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Products.Web.Controllers
{
    public class HomeController : Controller
    {
        //MVC Controller
        public ActionResult Index()
        {
            return View();
        }
    }
}
