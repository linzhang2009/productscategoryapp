﻿using Products.Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Products.Client.Controllers
{
     [Authorize]
    public class ProductsController : Controller
    {
        //
        // GET: /Products/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)   
        {
           // HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:56539/");
            HttpClientHandler handler = new HttpClientHandler();
            //handler.Credentials = new NetworkCredential("billy", "bill100");

            HttpClient client = new HttpClient(handler);
            client.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("basic",
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("bill:bill")));
            client.BaseAddress = new Uri("http://localhost:56539/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // get all products
            HttpResponseMessage response = client.GetAsync("api/products").Result;
            if (response.IsSuccessStatusCode)
            {
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                // Parse the response body
                var products = response.Content.ReadAsAsync<List<Product>>().Result.ToList();
                //Search and Sort
                ViewBag.CurrentSort = sortOrder;
                //Decending the list by ProductName
                ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "ProductName_desc" : "";
                //Decending the list by the Price
                ViewBag.PriceSort = sortOrder == "Price" ? "Price_desc" : "Price";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                
                //Search the string if it is included in the ProductName
                if (!String.IsNullOrEmpty(searchString))
                {
                    products = products.Where(x => x.ProductName.Contains(searchString)).ToList();
                }
                switch (sortOrder)
                {
                    //Decending by the Product Name:
                    case "ProductName_desc":
                        products = products.OrderByDescending(x => x.ProductName).ToList();
                        break;
                    //Oder By the Price
                    case "Price":
                        products = products.OrderBy(x => x.Price).ToList();
                        break;
                    //Decending by the Product Price:
                    case "Price_desc":
                        products = products.OrderByDescending(x => x.Price).ToList();
                        break;
                    default://Id acsending
                        products = products.OrderBy(x => x.ProductId).ToList();
                        break;
                }
                return View(products.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                throw new InvalidDataException(string.Format("{0} ({1})", response.StatusCode, response.ReasonPhrase));
            }
        }



        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection fields, Product product)
        {
            product.ProductName = fields["ProductName"];
            product.CategoryName = fields["Category"];
            product.Price = Convert.ToDecimal(fields["Price"]);
            product.UnitInStock = Convert.ToInt16(fields["UnitInStock"]);

            //call webApi to add new product
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:56539/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // add new  product  - post returns the created resource
            string apiurl = "api/products";
            HttpResponseMessage response = client.PostAsync<Product>(apiurl, product, new JsonMediaTypeFormatter()).Result;

            if (response.IsSuccessStatusCode)  // returns status 201 - created
            {
                //var products = response.Content.ReadAsAsync<IEnumerable<Product>>().Result;
                TempData["Product"] = String.Concat(" Product created successfully.." +
                    "with productId of " + product.ProductName.ToString());
            }
            else
            {
                TempData["Product"] = String.Concat(string.Format("{0} ({1})", response.StatusCode, response.ReasonPhrase));
            }
            return RedirectToAction("Index");
        }


        //Get: Product/Update/1
        public ActionResult Update(int productId)
        {
            Product product = new Product();
            //call webApi to add new product
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:56539/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            //Get:Product/1 
            string apiurl = "api/products" + "/" + productId;
            HttpResponseMessage response = client.GetAsync(apiurl).Result;
            if (response.IsSuccessStatusCode)  // returns status 201 - created
            {
                // Parse the response body
                product = response.Content.ReadAsAsync<Product>().Result;
                return View(product);
            }
            else
            {
                throw new InvalidDataException(string.Format("{0} ({1})", response.StatusCode, response.ReasonPhrase));
            }

        }


        //Put:Product/Update/1
        [HttpPost]
        public ActionResult Update(FormCollection fields, Product product)
        {
            product.ProductName = fields["ProductName"];
            product.CategoryName = fields["Category"];
            product.Price = Convert.ToDecimal(fields["Price"]);
            product.UnitInStock = Convert.ToInt16(fields["UnitInStock"]);

            //call webApi to add new product
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:56539/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // update product  - update does not return anything in webapi
            string apiurl = "api/products" +"/"+ product.ProductId.ToString();
            //Put
            HttpResponseMessage response = client.PutAsync<Product>(apiurl, product, new JsonMediaTypeFormatter()).Result;
            return RedirectToAction("Index");
        }

        // GET: Blogs/Delete/5
        public ActionResult Deleted(int ProductId)
        {

            // HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:56539/");
            HttpClientHandler handler = new HttpClientHandler();
            //handler.Credentials = new NetworkCredential("billy", "bill100");

            HttpClient client = new HttpClient(handler);
            client.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("basic",
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("bill:bill")));
            client.BaseAddress = new Uri("http://localhost:56539/");



            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // get product/id
            string apiurl = "api/products" + "/" + ProductId;
            HttpResponseMessage response = client.GetAsync(apiurl).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body
                var products = response.Content.ReadAsAsync<List<Product>>().Result.ToList();
                return View(products);
            }
            else
            {
                throw new InvalidDataException(string.Format("{0} ({1})", response.StatusCode, response.ReasonPhrase));
            }
        }

       

      
        public ActionResult Delete(int productId)
        {
            //call webApi to add new product
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:56539/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // delete product  - delete does not return anything in webapi
            string apiurl = "api/products" + "/" + productId;
            HttpResponseMessage response = client.DeleteAsync(apiurl).Result;

            return RedirectToAction("Index");
        }
    }
}
