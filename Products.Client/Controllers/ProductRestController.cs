﻿using Products.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RestSharp.Authenticators;
using PagedList;


namespace Products.Client.Controllers
{
    
    public class ProductRestController : Controller
    {
        //TODO:Move to configuration
        private const string localUrl = "http://localhost:56539/";
        //Basic Authentication
        private const string userName = "bill";
        private const string passWord = "bill";

        ProductClient _productClient = new ProductClient(localUrl, userName, passWord);       
     
        // GET: /ProductRest/ 
       // [Authorize(Roles="A")]
        [Authorize]
        public ActionResult Index(string sortOrder,string currentFilter,string searchString,int?page)      
        {
            ViewBag.CurrentSort = sortOrder;
            //Decending the list by ProductName
            ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "ProductName_desc" : "";
            //Decending the list by the Price
            ViewBag.PriceSort = sortOrder == "Price" ? "Price_desc" : "Price";
             
            if(searchString!=null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var products = _productClient.GetAll().ToList();
            //Search the string if it is included in the ProductName
            if(!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(x => x.ProductName.Contains(searchString)).ToList();
            }
            switch(sortOrder)
            {
                //Decending by the Product Name:
                case "ProductName_desc":
                    products = products.OrderByDescending(x => x.ProductName).ToList();
                    break;
                //Oder By the Price
                case "Price":
                    products = products.OrderBy(x => x.Price).ToList();
                    break;
                //Decending by the Product Price:
                case "Price_desc":
                    products = products.OrderByDescending(x => x.Price).ToList();
                    break;
                default://Id acsending
                    products = products.OrderBy(x => x.ProductId).ToList();
                    break;
            }
            //Paging:
            int pageSize = 5;
            int pageNumber = (page ?? 1);           
           
            return View(products.ToPagedList(pageNumber,pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }


        //Post for create
        [HttpPost]
        public ActionResult Create(FormCollection fields, Product product)
        {
            //
            product.ProductName = fields["ProductName"];
            product.CategoryName = fields["Category"];
            product.Price = Convert.ToDecimal(fields["Price"]);
            product.UnitInStock = Convert.ToInt16(fields["UnitInStock"]);

            product = _productClient.CreateProduct(product);
            return RedirectToAction("Index");
        }


        //Get: Product/Update/1
        //GetProduct
        public ActionResult Update(int productId)
        {
            var product = _productClient.GetProduct(productId);
            return View(product);
        }


        //Put:Product/Update/1
          [HttpPost]
        public ActionResult Update(FormCollection fields, Product product)
          {
              product.ProductName = fields["ProductName"];
              product.CategoryName = fields["Category"];
              product.Price = Convert.ToDecimal(fields["Price"]);
              product.UnitInStock = Convert.ToInt16(fields["UnitInStock"]);
              product = _productClient.PutProduct(product);
              return RedirectToAction("Index");
          }

          // GET: Blogs/Delete/5
          public ActionResult Delete(int productId)
          {              
              Product prod = _productClient.GetProduct(productId);              
              if (prod == null)
              {
                  return HttpNotFound();
              }
              return View(prod);
          }


          // Delete: Blogs/Delete/5          
          public ActionResult DeleteConfirmed(int ProductId)
          {
              _productClient.DeleteProduct(ProductId);
              return RedirectToAction("Index");
          }

    }
}
