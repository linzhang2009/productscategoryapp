﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Products.Library
{
    public class ProductClient
    {
        //properties
        private readonly RestClient _client;

        //constructors
        public ProductClient(string baseUrl, string userName, string passWord)
        {
            _client = new RestClient(baseUrl)
            {
                Authenticator = new HttpBasicAuthenticator(userName, passWord)
            };
        }


        //Get//api/products
        public List<Product> GetAll()
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = "api/products"
            };

            IRestResponse<List<Product>> response = _client.Execute<List<Product>>(request);
            List<Product> productList = response.Data;
            return productList;
        }


        //api/product/id
        public Product GetProduct(int productId)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/products/{0}", productId)
            };
            IRestResponse<Product> response = _client.Execute<Product>(request);
            return response.Data;
        }


        //api/products?cat=electronics
        public List<Product> GetProductsCategory(string cat)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/products?cat={0}", cat)
            };
            IRestResponse<List<Product>> response = _client.Execute<List<Product>>(request);
            List<Product> productList = response.Data;
            return productList;
        }


        //POST for Create  // api/products 
        public Product CreateProduct(Product prod)
        {
            RestRequest request = new RestRequest(Method.POST)
            {
                Resource = "api/products",
                RequestFormat = DataFormat.Json
            };
            request.AddBody(prod);

            IRestResponse<Product> response = _client.Execute<Product>(request);

            return response.Data;
        }


        //PUT FOR UPDATE // api/products/id 
        //returned from the API
        public Product PutProduct(Product prod)
        {
            RestRequest request = new RestRequest(Method.PUT)
            {
                Resource = "api/products/" + prod.ProductId,
                RequestFormat = DataFormat.Json
            };
            request.AddBody(prod);

            IRestResponse<Product> response = _client.Execute<Product>(request);

            return response.Data;
        }



        //Delete        
        public HttpStatusCode DeleteProduct(int ProductId)
        {
            var request = new RestRequest(Method.DELETE)
            {
                Resource = "api/products/" + ProductId
            };
            var response = _client.Execute<Product>(request);
            return response.StatusCode;
        }

        /*
        public void DeleteProduct(int ProductId)
        {
            RestRequest request = new RestRequest(Method.DELETE)
            {
                Resource = "api/products/" + ProductId,
                RequestFormat = DataFormat.Json
            };
             _client.Execute<Product>(request);
            
        }*/


    }
}
