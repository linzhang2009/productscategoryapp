﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;



namespace Products.Library.ProductsRepository
{
    public class CategoriesRepository 
    {

        //Get the Categories by Id
        public Category GetCategoryById(int id)
        {
            using (var category = new ProductsEntities())
            {

                var ca = category.Categories.Where(x => x.CategoryId == id).FirstOrDefault();
                if (ca == null)
                {
                    throw new InvalidDataException("product Does Not Exsit");
                }
                return ca;
            }
        }

        public List<Category> GetAllCategories()
        {
            using (var category = new ProductsEntities())
            {
                var list = category.Categories.ToList();
                return list;
            }
        }
       
    }
}