﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Products.Library.ProductsRepository
{
    public class ProductsRepository
    {
        //Get the product by Id
        public Product GetProductById(int id)
        {
            using (var product = new ProductsEntities())
            {

                var pd = product.Products.Where(x => x.ProductId == id).FirstOrDefault();
                if (pd == null)
                {
                    throw new InvalidDataException("product Does Not Exsit");
                }
                return pd;
            }
        }

        public List<User> GetAllUser()
        {
            using (var users = new ProductsEntities())
            {
                var userlist = users.Users.ToList();
                return userlist;
            }
        }

        public User GetUserByName(string userName,string password)
        {
            using (var users = new ProductsEntities())
            {
                var user = users.Users.Where(x => x.UserName == userName && x.PassWord == password).FirstOrDefault();
                if(user==null)
                {
                    throw new InvalidDataException("User Does Not Exsit");
                }
                return user;
            }
        }


        public List<Product> GetAllProducts()
        {
            using (var product = new ProductsEntities())
            {
                var list = product.Products.ToList();
                return list;
            }

        }

        public List<Product> GetProductsByCategory(string cat)
        {
            using (var product = new ProductsEntities())
            {
                var res = product.Products.Where(x => x.CategoryName == cat).ToList();
                if (res == null)
                {
                    throw new InvalidDataException("product Does Not Exsit");
                }
                return res;
            }
        }

        public Product Add(Product p)
        {
            using (var product = new ProductsEntities())
            {
                product.Products.Add(p);
                product.SaveChanges();
                return p;
            }
        }

        public Product Update(Product updateProduct)
        {
            using (var product = new ProductsEntities())
            {
                var res = product.Products.Where(x => x.ProductId == updateProduct.ProductId).FirstOrDefault();
                if (res != null)
                {
                    res.ProductName = updateProduct.ProductName;
                    res.Price = updateProduct.Price;
                    res.UnitInStock = updateProduct.UnitInStock;
                    res.CategoryName = updateProduct.CategoryName;
                    product.SaveChanges();
                }
                else
                {
                    throw new InvalidDataException("Product Does Not Exsit");
                }
                return updateProduct;
            }

        }


        public void Delete(int ProductId)
        {
            using (var product = new ProductsEntities())
            {
                var p = product.Products.Where(x => x.ProductId == ProductId).FirstOrDefault();
                if (p != null)
                {
                    product.Products.Remove(p);
                    product.SaveChanges();

                }
                else
                {
                    throw new InvalidDataException("Product Does Not Exsit");
                }
            }
        }

    }
}
